const express = require("express");
const app = express();

var server = require('http').createServer(app);
var io = require("socket.io").listen(server);

var users = [];
var connection = [];

server.listen(process.env.PORT || 3000);
console.log("Server running");

app.get('/', (req, res) => {
    res.sendFile(__dirname + "/index.html"); //lo mando alla pagina index.html
});


//gestione delle connessioni tramite socket
io.sockets.on('connection', (socket)=> {
    connection.push(socket);    //aggiungo una connessione alla lista delle connessioni
    console.log('Connected: %s socket connected!', connection.length);


    //quando c'è la disconnessione:
    socket.on('disconnect', (data) => {
        connection.splice(connection.indexOf(socket), 1);   //elimino il socket dalla lista delle connessioni
        console.log("Disconnected: %s sockets connected", connection.length);
    });

    //send message
    socket.on("send-message", (data) => {
        // console.log(data);   //per vedere cosa stampava data
        io.sockets.emit('new-message', { msg: data})
    });

});
